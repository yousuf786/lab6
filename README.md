#include <iostream>;
#include <random>;
#include <stdio.h>;
#include <stdlib.h>  ;   
#include <time.h> ;
#include <vector>;
#include <algorithm> ;   
using namespace std;

void swap(int &a, int &b) {
	int c = a;
	a = b;
	b = c;
}

bool moveMin(vector<int> &in, vector<int> &out) {

	clock_t time = clock();

	out = in;

	bool d = true;
	int max = 0, i = 1;

	while (d) {

		d = false;

		for (int i = 1; i < out.size() - i; i++) {

			if (out[max] > out[i]) {
				d = true;
			}
			else max = i;

		}

		swap(out[max], out[out.size() - i - 1]);
		i++;
		max = 0;


	}

	Runtime.push_back((double)(clock() - time) / CLOCKS_PER_SEC);

	return true;

}

bool testCase(vector<int> BubbleSort, vector<int> Sort) {

	for (int i = 0; i < BubbleSort.size(); i++) {
		if (BubbleSort[i] != Sort[i]) return false;
	}

	return true;

}

bool testmoveMin() {

	vector<int> in;
	vector<int> out = in;

	int a;


	for (int i = 0; i < 10; i++) {
		a = rand() % 100;
		in.push_back(a);
		cout << a << " ";
	}

	cout << endl;

	moveMin(in, out);

	cout << "new Vector: ";

	for (int i : out) cout << i << " ";

	cout << endl;

	vector<int> builtInSort = in;

	sort(builtInSort.begin(), builtInSort.end());

	cout << "sort function Vector: ";

	for (int i : builtInSort) cout << i << " ";

	cout << endl;



	bool match = testCase(out, builtInSort);



	if (match) cout << "Vectors matched\n";
	else cout << "Vectors not matched\n";



	return true;

}

void random() {

	int randomdice;

	//setting time to NULL
	srand(time(NULL));

	for (int i = 0; i < 6; i++) {

		randomdice = rand() % 6 + 1;

		cout << randomdice << " ";
	}
}





int main(int argc, char **argv) {

	//calling function
	random();
	vector<int> b;
	
	
	
for (int i = 0; i < 5; i++) {
		srand(time(NULL) + i);
		testmoveMin();
	}
	
	for (int i = 0; i < 100; i++) {
		srand(time(NULL) + i);
		testmoveMin();
	}

	double best = *min_element(Runtime.begin(), Runtime.end());
	double worst = *max_element(Runtime.begin(), Runtime.end());
	double sum = 0;

	for (double a : Runtime) sum += a;
	cout << "Average Case: " << sum / Runtime.size() << endl;
	cout << "Best Case: " << best << endl;
	cout << "Worst Case: " << worst << endl;


	system("pause");

	return 0;
	}